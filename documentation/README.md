# Welcome to CSITC

Cumulus (and SONiC) in the Cloud provides you a full datacenter fabric with all of the features and functionality of Cumulus Linux running on real networking hardware.

Your lab consists of:

* 6 Cumulus Linux Leaf switches
* 2 Cumulus Linux Spine switches (spine01, spine02)
* 2 SONiC Spine switches (spine03, spine04)
* 8 Ubuntu servers
* 2 \"firewall\" devices to isolate tenants
* Complete out of band management networking

You can view the topology in the \"Nodes\" section below.

## Provisioning the Lab

Currently your lab is unconfigured. You are welcome to jump to the Connecting to the Lab portion of this guide if you already know what you're doing.

Otherwise, on the bar to the left, under the section **Learn** you can use our self-paced video on demand training by clicking **Cumulus Linux On Demand**. Alternatively, this lab guide will walk you through using an EVPN network.

To get started, the automation to provision the network needs to be cloned to the `oob-mgmt-server`. Log into the `oob-mgmt-server` and and issue the following command:

```
git clone https://gitlab.com/cumulus-consulting/goldenturtle/sonic-spines-demo.git && cd sonic-spines-demo
```

Move into the automation directory
```
$ cd automation
```

Run the Ansible restore automation for the Cumulus nodes
```
$ ansible-playbook -i inventories/pod1/ playbooks/restore_files.yml
```

Run the Ansible restore automation for the SONiC nodes

```
$ ansible-playbook -i inventories/pod1/ playbooks/restore_sonic.yml
```

Following the above steps will deploy an EVPN Symmetric mode solution into the fabric.

This guide will walk through a number of commands to verify various parts of the configuration and show a few features of Cumulus Linux.

## Accessing the Lab

To access the lab, click on the node below to connect to the console.

The username and password to access the OOB Server is:

**Username**: `cumulus`

**Password**: `CumulusLinux!`

This username and password combination is also used on all other devices in the lab.

You can access any other device in the network from the OOB server via SSH to the device hostname. For example `ssh leaf01` or `ssh server06`.

Click **Next** to see how to verify your lab connectivity.

<!-- AIR:page -->

## Verifying the Lab

### Verifying Connectivity

#### Verify Connectivity with LLDP

First, we can do a simple validation of the connectivity in our lab.

1. Connect to `spine03`

**SONiC Default Login:**

| Credential | Value |
|----------|:--------|
| Username | admin |
| Password | sonic |

2. Run `net show lldp` to view the LLDP peers of spine01

We should see the `oob-mgmt-switch` on eth0, leafs01-04 and then border01-02.

```
cumulus@spine01:mgmt-vrf:~$ net show lldp

LocalPort  Speed  Mode     RemoteHost       RemotePort
---------  -----  -------  ---------------  ----------
eth0       1G     Mgmt     oob-mgmt-switch  swp14
swp1       1G     Default  leaf01           swp51
swp2       1G     Default  leaf02           swp51
swp3       1G     Default  leaf03           swp51
swp4       1G     Default  leaf04           swp51
swp5       1G     Default  border01         swp51
swp6       1G     Default  border02         swp51
```

#### Verify Connectivity with NetQ

Cumulus NetQ is a streaming telemetry tool that is installed on all devices in this lab. We can leverage NetQ to see data about any device in the network from anywhere. For example, while still on `spine01` run the command:

`netq leaf04 show lldp`

This will show us all of the LLDP peers on leaf04. You can do this for any device in the entire lab, including the servers.

```
cumulus@spine01:mgmt-vrf:~$ netq leaf04 show lldp

Matching lldp records:
Hostname          Interface                 Peer Hostname     Peer Interface
----------------- ------------------------- ----------------- ------------------------
leaf04            swp49                     leaf03            swp49
leaf04            swp2                      server05          mac:ca:8c:e5:47:35:55
leaf04            swp53                     spine03           swp4
leaf04            swp50                     leaf03            swp50
leaf04            swp3                      server06          mac:e2:89:01:3e:19:d8
leaf04            swp52                     spine02           swp4
leaf04            eth0                      oob-mgmt-switch   swp13
leaf04            swp1                      server04          mac:2a:a6:4a:54:01:7e
leaf04            swp54                     spine04           swp4
leaf04            swp51                     spine01           swp4
```

Click **Next** to view the routing information configured in the lab.

<!-- AIR:page -->

## Verifying Routing

Next, check that the BGP underlay is operational.

From `spine01` use the command:

`net show bgp summary`

To view the summary output of the BGP neighbors.

```
cumulus@spine01:mgmt-vrf:~$ net show bgp sum
show bgp ipv4 unicast summary
=============================
BGP router identifier 10.10.10.101, local AS number 65199 vrf-id 0
BGP table version 18
RIB entries 19, using 2888 bytes of memory
Peers 6, using 116 KiB of memory
Peer groups 1, using 64 bytes of memory

Neighbor        V         AS MsgRcvd MsgSent   TblVer  InQ OutQ  Up/Down State/PfxRcd
leaf01(swp1)    4      65101    6123    6150        0    0    0 05:00:58            3
leaf02(swp2)    4      65101    6121    6149        0    0    0 05:00:59            3
leaf03(swp3)    4      65102    6128    6149        0    0    0 05:00:59            3
leaf04(swp4)    4      65102    6128    6149        0    0    0 05:00:59            3
border01(swp5)  4      65254    6139    6149        0    0    0 05:00:59            3
border02(swp6)  4      65254    6133    6138        0    0    0 05:00:57            3

Total number of neighbors 6


show bgp ipv6 unicast summary
=============================
% No BGP neighbors found


show bgp l2vpn evpn summary
===========================
BGP router identifier 10.10.10.101, local AS number 65199 vrf-id 0
BGP table version 0
RIB entries 35, using 5320 bytes of memory
Peers 6, using 116 KiB of memory
Peer groups 1, using 64 bytes of memory

Neighbor        V         AS MsgRcvd MsgSent   TblVer  InQ OutQ  Up/Down State/PfxRcd
leaf01(swp1)    4      65101    6123    6150        0    0    0 05:00:59           18
leaf02(swp2)    4      65101    6121    6149        0    0    0 05:01:00           18
leaf03(swp3)    4      65102    6128    6149        0    0    0 05:01:00           18
leaf04(swp4)    4      65102    6128    6149        0    0    0 05:01:00           18
border01(swp5)  4      65254    6139    6149        0    0    0 05:01:00            6
border02(swp6)  4      65254    6133    6138        0    0    0 05:00:58            6

Total number of neighbors 6
```

At the top we see the `IPv4 Address Family` which is the BGP Address family used to carry IP addresses used in the EVPN underlay, like loopback IPs.

Next we see the `l2vpn evpn Address Family` which are the endpoint MAC and IP routes. This would include data like the server MAC addresses and any subnets the servers are members of. We will look more closely at EVPN shortly.

### Using NetQ for BGP

Similar to LLDP we can look at the BGP state information from any device. While still on `spine01` run the command

`netq leaf01 show bgp`

You see that leaf01 has BGP peers with all four spines and it's partner leaf, leaf02.

```
cumulus@spine01:mgmt-vrf:~$ netq leaf01 show bgp

Matching bgp records:
Hostname          Neighbor                     VRF             ASN        Peer ASN   PfxRx
----------------- ---------------------------- --------------- ---------- ---------- ------------
leaf01            swp53(spine03)               default         65101      65199      7/-/36
leaf01            swp51(spine01)               default         65101      65199      7/-/36
leaf01            swp54(spine04)               default         65101      65199      7/-/36
leaf01            swp52(spine02)               default         65101      65199      7/-/36
leaf01            peerlink.4094(leaf02)        default         65101      65101      12/-/-
```

NetQ also provides the ability to dynamically check the state of the entire fabric with a single command. For example, we can cause a failure of a BGP peer on `spine01` with the commands:

```
net add bgp neighbor swp1 shutdown
net commit
```

We can verify the neighbor on `swp1` has been shutdown with the command:

`net show bgp summary`

In the output we should see the peer `leaf01(swp1)` in the `Idle (Admin)` state.

```
Neighbor        V         AS MsgRcvd MsgSent   TblVer  InQ OutQ  Up/Down State/PfxRcd
leaf01(swp1)    4      65101    7426    7455        0    0    0 00:00:04 Idle (Admin)
leaf02(swp2)    4      65101    7426    7456        0    0    0 06:06:13            3
leaf03(swp3)    4      65102    7444    7456        0    0    0 06:06:13            3
leaf04(swp4)    4      65102    7444    7456        0    0    0 06:06:13            3
border01(swp5)  4      65254    7455    7456        0    0    0 06:06:13            3
border02(swp6)  4      65254    7443    7445        0    0    0 06:06:11            3
```

Now with Netq we can check the state of BGP across the fabric with the command:

`netq check bgp`

NetQ will look at the state of BGP on every node in the network and report back any failures it has found. It may take up to 10 seconds for NetQ to report the failures on both `leaf01` and `spine01`

```
cumulus@spine01:mgmt-vrf:~$ netq check bgp
bgp check result summary:

Checked nodes       : 10
Total nodes         : 10
Rotten nodes        : 0
Failed nodes        : 2
Warning nodes       : 0

Additional summary:
Total Sessions      : 54
Failed Sessions     : 2


Session Establishment Test   : 0 warnings, 2 errors
Address Families Test        : passed
Router ID Test               : passed


Session Establishment Test details:
Hostname          VRF             Peer Name         Peer Hostname     Reason
----------------- --------------- ----------------- ----------------- ---------------------------------------------
leaf01            default         swp51             spine01           BGP session with peer spine01 (swp51 vrf defa
                                                                      ult) failed, reason: Peer closed the session
spine01           default         swp1              leaf01            BGP session with peer leaf01 (swp1 vrf defaul
                                                                      t) failed, reason: Admin. shutdown
```

NetQ also has the ability to look at the state of the environment at any point in the past. We can do this for both the `netq show` and `netq check` commands.

For example, what was the output of `netq check bgp` 5 minutes ago, before we shut the neighbor down? We can do this with the command:

`netq check bgp around 5m`

This will look at the historic data NetQ has collected and report the state.

```
cumulus@spine01:mgmt-vrf:~$ netq check bgp around 5m
bgp check result summary:

Checked nodes       : 10
Total nodes         : 10
Rotten nodes        : 0
Failed nodes        : 0
Warning nodes       : 0

Additional summary:
Total Sessions      : 54
Failed Sessions     : 0


Session Establishment Test   : passed
Address Families Test        : passed
Router ID Test               : passed
```

We can also see the specific BGP data at that time with the command:

`netq spine01 show bgp around 5m`

Again, we will look at the state of the BGP neighbors on `spine01`, but in this instance, we are looking at the state 5 minutes ago, and not the live view.

```
cumulus@spine01:mgmt-vrf:~$ netq spine01 show bgp around 5m

Matching bgp records:
Hostname          Neighbor                     VRF             ASN        Peer ASN   PfxRx
----------------- ---------------------------- --------------- ---------- ---------- ------------
spine01           swp5(border01)               default         65199      65254      3/-/6
spine01           swp1(leaf01)                 default         65199      65101      3/-/12
spine01           swp6(border02)               default         65199      65254      3/-/6
spine01           swp2(leaf02)                 default         65199      65101      3/-/12
spine01           swp3(leaf03)                 default         65199      65102      3/-/12
spine01           swp4(leaf04)                 default         65199      65102      3/-/12
```

### BGP Unnumbered

This lab is built using [BGP Unnumbered](https://docs.cumulusnetworks.com/cumulus-linux-41/Layer-3/Border-Gateway-Protocol-BGP/#bgp-unnumbered-interfaces). BGP unnumbered allows us to build BGP peerings without assigning IP addresses on the point-to-point routing links.

On `spine01` we earlier saw that BGP peers exist on swp1, swp2, swp3 and swp4 (swp ports are the indication of front panel ports on switches with Cumulus Linux).

We can look at the interface configuration to see that no IP addresses have been assigned. First, look at the interfaces with the command:

`net show interface`

Notice that an IP address has been assigned to the `lo` loopback address and management `eth0` interface, but no IPs are assigned to any of the swp ports.

```
cumulus@spine01:mgmt-vrf:~$ net show interface
State  Name  Spd  MTU    Mode      LLDP                     Summary
-----  ----  ---  -----  --------  -----------------------  ---------------------------
UP     lo    N/A  65536  Loopback                           IP: 127.0.0.1/8
       lo                                                   IP: 10.10.10.101/32
       lo                                                   IP: ::1/128
UP     eth0  1G   1500   Mgmt      oob-mgmt-switch (swp14)  Master: mgmt(UP)
       eth0                                                 IP: 192.168.200.21/24(DHCP)
UP     swp1  1G   9216   Default   leaf01 (swp51)
UP     swp2  1G   9216   Default   leaf02 (swp51)
UP     swp3  1G   9216   Default   leaf03 (swp51)
UP     swp4  1G   9216   Default   leaf04 (swp51)
UP     swp5  1G   9216   Default   border01 (swp51)
UP     swp6  1G   9216   Default   border02 (swp51)
UP     mgmt  N/A  65536  VRF                                IP: 127.0.0.1/8
```

We can also look at the Linux configuration file of the network interfaces in `/etc/network/interfaces` to see that no IP addresses have been configured.

On `spine01` run the command:

`cat /etc/network/interfaces`

to view the entire configuration file. At the bottom, you see the interface definitions with no addresses defined

```
# Edited for brevity
###############
# Fabric Links
###############

auto swp1
iface swp1
    alias fabric link

auto swp2
iface swp2
    alias fabric link

auto swp3
iface swp3
    alias fabric link
```

BGP unnumbered relies on automatically assigned IPv6 Link Local addresses on the interfaces to connect to peers. We can see this information by looking at a specific peer on `spine01` with the command

`net show bgp swp1`

This shows the BGP peer information, but at the bottom of this output we can see the `Local host` and `Foreign host` addresses

```
cumulus@spine01:mgmt-vrf:~$ net show bgp neighbor swp1
BGP neighbor on swp1: fe80::a4dc:bcff:fea5:fe28, remote AS 65101, local AS 65199, external link
Hostname: leaf01
# Cut for brevity
Local host: fe80::cae:46ff:fe3d:f8d2, Local port: 40349
Foreign host: fe80::a4dc:bcff:fea5:fe28, Foreign port: 179
Nexthop: 10.10.10.101
Nexthop global: fe80::cae:46ff:fe3d:f8d2
Nexthop local: fe80::cae:46ff:fe3d:f8d2
BGP connection: shared network
BGP Connect Retry Timer in Seconds: 10
Read thread: on  Write thread: on
```

The `local host` is our IPv6 address and `Foreign host` is the peer's address.

Cumulus also automatically enables support for BGP hostnames as defined in the IETF draft, [draft-walton-bgp-hostname-capability](https://tools.ietf.org/html/draft-walton-bgp-hostname-capability). Cumulus Network hosts automatically provide their LLDP hostname as part of the BGP neighbor process. This makes troubleshooting and operations much easier to see which peer maps to which interface in the topology. We can see this in any BGP output, for example, the summary BGP output on `spine01`

`net show bgp summary`

This again shows the list of BGP peers. Since we are using BGP unnumbered the interfaces are listed in parenthesis, to the left are the learned hostnames.

```
cumulus@spine01:mgmt-vrf:~$ net show bgp sum
show bgp ipv4 unicast summary
=============================
BGP router identifier 10.10.10.101, local AS number 65199 vrf-id 0
BGP table version 18
RIB entries 19, using 2888 bytes of memory
Peers 6, using 116 KiB of memory
Peer groups 1, using 64 bytes of memory

Neighbor        V         AS MsgRcvd MsgSent   TblVer  InQ OutQ  Up/Down State/PfxRcd
leaf01(swp1)    4      65101    7246    7273        0    0    0 05:57:08            3
leaf02(swp2)    4      65101    7244    7272        0    0    0 05:57:09            3
leaf03(swp3)    4      65102    7251    7272        0    0    0 05:57:09            3
leaf04(swp4)    4      65102    7251    7272        0    0    0 05:57:09            3
border01(swp5)  4      65254    7262    7272        0    0    0 05:57:09            3
border02(swp6)  4      65254    7257    7262        0    0    0 05:57:07            3

Total number of neighbors 6
```

<!-- AIR:page -->

## Layer 2 Extension with VXLAN

A common reason for networks to deploy VXLAN-EVPN is to provide layer 2 extension, between racks, over a routed layer 3 core. In this environment a number of the servers in various racks are within the same layer 2 subnet. We will focus on `server01` and `server04`.

Server01 has the IP address `10.1.10.101`.
Server04 has the IP address `10.1.10.104`.

Since they are in different racks, with a layer 3 core, VxLAN will be used to communicate between them.

We can use NetQ to view information about the servers as well as the network. First, let's fine the MAC address of `server04`.

From any device run the command:

`netq server01 show ip neighbors`

From this output we can collect the MAC address of `server04`

```
Matching neighbor records:
IP Address                Hostname          Interface                 MAC Address
------------------------- ----------------- ------------------------- ------------------
10.1.10.104               server01          uplink                    ea:ef:27:44:4d:86
```

This output shows us that server01 has an IP neighbor for `10.1.10.104` at MAC address `ea:ef:27:44:4d:86`

Next we can look at the MAC Address Table of `leaf01`, where `server01` is connected. Use the MAC address of `server04` in the following command:

`netq leaf01 show macs ea:ef:27:44:4d:86`

This shows us that the MAC address is learned via `vni30010` from `leaf04` in VLAN 10.

```
cumulus@spine01:mgmt-vrf:~$ netq leaf01 show macs ea:ef:27:44:4d:86

Matching mac records:
Origin MAC Address        VLAN   Hostname          Egress Port
------ ------------------ ------ ----------------- ------------------------------
no     ea:ef:27:44:4d:86  10     leaf01            vni30010:leaf04
```

If we want to see exactly what path a packet would take through the network we can use `netq trace` command, which will show both underlay and overlay paths.

`netq trace ea:ef:27:44:4d:86 vlan 10 from leaf01 pretty`

Note, it may take up to 30 seconds for the command to complete, due to the resources allocated to the NetQ server in this lab environment.

```
cumulus@spine01:mgmt-vrf:~$ time netq trace ea:ef:27:44:4d:86 vlan 10 from leaf01 pretty
Number of Paths: 6
Number of Paths with Errors: 0
Number of Paths with Warnings: 6
  Path: 1 Underlay mtu 9216 at leaf01:swp54 not enough encap headroom
  Path: 2 Underlay mtu 9216 at leaf01:swp54 not enough encap headroom
  Path: 3 Underlay mtu 9216 at leaf01:swp53 not enough encap headroom
  Path: 4 Underlay mtu 9216 at leaf01:swp53 not enough encap headroom
  Path: 5 Underlay mtu 9216 at leaf01:swp52 not enough encap headroom
  Path: 6 Underlay mtu 9216 at leaf01:swp52 not enough encap headroom
Path MTU: 9216

 leaf01 vni: 30010 swp54 -- swp1 spine04 swp4 -- swp54 vni: 30010 leaf04 bond1 -- uplink server04
                   swp54 -- swp1 spine04 swp3 -- swp54 vni: 30010 leaf03 bond1 -- uplink server04
 leaf01 vni: 30010 swp53 -- swp1 spine03 swp4 -- swp53 vni: 30010 leaf04 bond1 -- uplink server04
                   swp53 -- swp1 spine03 swp3 -- swp53 vni: 30010 leaf03 bond1 -- uplink server04
 leaf01 vni: 30010 swp52 -- swp1 spine02 swp4 -- swp52 vni: 30010 leaf04 bond1 -- uplink server04
                   swp52 -- swp1 spine02 swp3 -- swp52 vni: 30010 leaf03 bond1 -- uplink server04
```

First, we see warnings that the MTU of the server and core are the same, 9216. A full sized frame from server01 of 9216 bytes would not be supported when the VXLAN encapsulation is added. This is an example of how NetQ can proactively identify MTU issues in the network.

Next, we see that `leaf01` has three paths available, via `swp54`, `swp53` and `swp52`. Remember, earlier we disabled the BGP peer between `spine01` and `leaf01`. Thankfully NetQ allows us to go back in time with trace commands as well with the `around` keyword.

```
cumulus@spine01:mgmt-vrf:~$ netq trace ea:ef:27:44:4d:86 vlan 10 from leaf01 around 10m pretty
Number of Paths: 8
Number of Paths with Errors: 0
Number of Paths with Warnings: 8
  Path: 1 Underlay mtu 9216 at leaf01:swp54 not enough encap headroom
  Path: 2 Underlay mtu 9216 at leaf01:swp54 not enough encap headroom
  Path: 3 Underlay mtu 9216 at leaf01:swp53 not enough encap headroom
  Path: 4 Underlay mtu 9216 at leaf01:swp53 not enough encap headroom
  Path: 5 Underlay mtu 9216 at leaf01:swp52 not enough encap headroom
  Path: 6 Underlay mtu 9216 at leaf01:swp52 not enough encap headroom
  Path: 7 Underlay mtu 9216 at leaf01:swp51 not enough encap headroom
  Path: 8 Underlay mtu 9216 at leaf01:swp51 not enough encap headroom
Path MTU: 9216

 leaf01 vni: 30010 swp54 -- swp1 spine04 swp4 -- swp54 vni: 30010 leaf04 bond1 -- uplink server04
                   swp54 -- swp1 spine04 swp3 -- swp54 vni: 30010 leaf03 bond1 -- uplink server04
 leaf01 vni: 30010 swp53 -- swp1 spine03 swp4 -- swp53 vni: 30010 leaf04 bond1 -- uplink server04
                   swp53 -- swp1 spine03 swp3 -- swp53 vni: 30010 leaf03 bond1 -- uplink server04
 leaf01 vni: 30010 swp52 -- swp1 spine02 swp4 -- swp52 vni: 30010 leaf04 bond1 -- uplink server04
                   swp52 -- swp1 spine02 swp3 -- swp52 vni: 30010 leaf03 bond1 -- uplink server04
 leaf01 vni: 30010 swp51 -- swp1 spine01 swp4 -- swp51 vni: 30010 leaf04 bond1 -- uplink server04
                   swp51 -- swp1 spine01 swp3 -- swp51 vni: 30010 leaf03 bond1 -- uplink server04
```

Here we see the L2+VXLAN trace before we shut down the BGP peer and we can see all 4 uplinks in use.
Continuing down the path, from each of the spines the traffic will be ECMP load shared between `leaf03` and `leaf04` before arriving on `server04`.

Click **Next** to see where you can go from here with your lab or where you can learn more about Cumulus Linux and Cumulus NetQ.

<!-- AIR:page -->

## What's Next

From here you can continue to explore the lab. Break things, configure anything you'd like or try a different demo environment.

If you'd like to learn more you can try our self-paced [virtual test drive](https://cumulusnetworks.com/lp/cumulus-linux-on-demand/) course that will teach you the basics of Cumulus Linux with additional hands on labs.

You can also use the NetQ graphical interface in the menu on the left, which provides access to the NetQ data we've already seen and so much more!

If you want to get more involved, join our [Community Slack](http://slack.cumulusnetworks.com). If you have any questions or want more information about Cumulus Linux, Cumulus NetQ or anything else, please contact us by emailing [sales@cumulusnetworks.com](mailto:sales@cumulusnetworks.com).

