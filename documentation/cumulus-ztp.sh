#!/bin/bash

function error() {
  echo -e "e[0;33mERROR: The Zero Touch Provisioning script failed while running the command $BASH_COMMAND at line $BASH_LINENO.e[0m" >&2
}
trap error ERR

# unexpire cumulus user password (new for 4.2)
passwd -x 99999 cumulus
echo 'cumulus:CumulusLinux!' | chpasswd

# make user cumulus passwordless sudo
echo "cumulus ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers.d/10_cumulus

reboot
exit 0
#CUMULUS-AUTOPROVISIONING
