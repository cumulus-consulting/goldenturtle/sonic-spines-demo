# Cumulus and SONIC In The Cloud

This repository is meant to work in conjunction with the `Cumulus and SONiC in the Cloud` topology on the NVIDIA Cumulus Air platform.

This architecture is built on the cldemo2 topology: https://gitlab.com/cumulus-consulting/goldenturtle/cldemo2

This architecture has `spine03` and `spine04` as SONIC images with all the remaining nodes as Cumulus Linux images.

This demo is an unconfigured environment of all Cumulus Linux and SONIC nodes.

For a pre-configured demo that uses the same topology but implements a layer 3 fabric, use the demo marketplace simulation named `VXLAN EVPN fabric with Cumulus and SONiC`

## Login Details

The login for the Out of Band Server;

| Credential | Value |
|----------|:--------|
| Username | cumulus |
| Password | CumulusLinux! |

The login for the Cumulus Linux nodes:

| Credential | Value |
|----------|:--------|
| Username | cumulus |
| Password | cumulus |

This is the default username and password for Cumulus Linux images.


The login for the SONIC nodes:

| Credential | Value |
|----------|:--------|
| Username | admin |
| Password | YourPaSsWoRd |

This is the default username and password for SONIC images, as set by the upstream repository.

